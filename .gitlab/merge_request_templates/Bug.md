# Bug outline
<!-- what is the bug about? -->

# Technical outline
<!-- what caused the bug in terms of code and how to fix it -->

# How to reproduce it in production
<!-- steps to reproduce this bug -->

# Aside changes made
<!-- list of things changed that's not part of the bug fix -->

# Risks
<!-- list of any risks to look out for -->
