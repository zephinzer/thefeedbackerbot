FROM node:10.15.0-alpine AS base
RUN apk update --no-cache
RUN apk upgrade --no-cache
WORKDIR /app

FROM base AS development_base
RUN apk add --no-cache bash curl git make
RUN npm i -g nodemon
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install

FROM development_base AS development
COPY . /app
VOLUME [ "/app/src" ]
EXPOSE $PORT
CMD [ "npm", "start" ]

FROM base AS production
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm install --production
COPY . /app
VOLUME [ "/app/src" ]
EXPOSE $PORT
CMD [ "npm", "start" ]
