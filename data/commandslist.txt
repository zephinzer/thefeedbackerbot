create - Create a team
join - Join an existing team
leave - Leave a team you are in
me - View information about yourself
mine - View feedback given to you
feedback - Give feedback to someone