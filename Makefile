-include Makefile.properties

init: # initialises the development environment
	@mkdir -p node_modules
	$(MAKE) log.info MSG="Installing dependencies..."
	$(MAKE) deps
	$(MAKE) log.info MSG="Starting development environment..."
	$(MAKE) start
	$(MAKE) log.info MSG="Showing application logs in follow mode..."
	$(MAKE) logs
firebase: # generates the base64 version of the firebase service account credentials
	@cat $(CURDIR)/secrets/firebase.json | base64 -w 0 > $(CURDIR)/secrets/firebase.json.b64
start: # starts the application in the development environment
	@UID=$$(id -u) docker-compose up --build -d -V bot
	@$(MAKE) logs
seeds: # seeds the database
	docker exec thefeedbackerbot node /app/seed/index.js
shell: # creates a shell into the application environment
	@docker exec -it thefeedbackerbot /bin/bash
logs: # displays logs for the 
	@docker logs -f thefeedbackerbot
stop: # stops the application
	@UID=$$(id -u) docker-compose down
deps: # triggers dependency installation in the development environmnet
	@UID=$$(id -u) docker-compose run install_dependencies
build: verify.makefile.properties
	$(MAKE) build.production
	$(MAKE) build.development
build.production:
	@docker build \
		--target production \
		-t $(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		.
build.development:
	@docker build \
		--target development \
		-t $(IMAGE_NAMESPACE)/$(IMAGE_NAME)-dev:latest \
		.
publish: build
	@$(MAKE) publish.latest
publish.latest: verify.makefile.properties
publish.latest.production: build.production
	@docker tag \
		$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest
	@docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest
publish.latest.development: build.development
	@docker tag \
		$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-dev:latest \
		$(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-dev:latest
	@docker push $(IMAGE_REGISTRY)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME)-dev:latest
release: build

version.get: # retrieves the latest version we are at
	@docker run -v "$(CURDIR):/app" zephinzer/vtscripts:latest get-latest -q
version.bump: # bumps the version by 1: specify VERSION as "patch", "minor", or "major", to be specific about things
	@docker run -v "$(CURDIR):/app" zephinzer/vtscripts:latest iterate ${VERSION} -i

verify.makefile.properties:
	@if ! [ -f $(CURDIR)/Makefile.properties ]; then \
		cp $(CURDIR)/sample.properties $(CURDIR)/Makefile.properties; \
		$(MAKE) log.error MSG="Makefile.properties wasn't found so it was created. Re-run the same command to complete the action."; \
		exit 1; \
	fi
log.debug: # debug logging
	-@printf -- "\033[36m\033[1m_ [DEBUG] ${MSG}\033[0m\n"
log.info: # info logging
	-@printf -- "\033[32m\033[1m>  [INFO] ${MSG}\033[0m\n"
log.warn: # warn logging
	-@printf -- "\033[33m\033[1m?  [WARN] ${MSG}\033[0m\n"
log.error: # error logging
	-@printf -- "\033[31m\033[1m! [ERROR] ${MSG}\033[0m\n"