const database = require('../src/database');
const faker = require('faker');

const logger = require('../src/logger');
const {User} = require('../src/users');
const {Team} = require('../src/teams');

const userIdList = [
  '00000000a',
  '00000000b',
  '00000000c',
  '00000000d',
  '00000000e',
  '00000000f',
];

const teamIdList = [
  '000001',
  '000002',
  '000003',
  '000004',
];

const userList = userIdList
  .map((id) => {
    return new User(database, {id, name: faker.name.firstName()});
  })
  .map((user) => user.create().then((v) => ({[v.id]: v})));

const teamList = teamIdList
  .map((id) => {
    return new Team(database, {id, name: faker.company.companyName()});
  })
  .map(
    (team) => {
      const randomUserId = userIdList[Math.floor(Math.random() * userIdList.length)];
      logger.info(randomUserId);
      return team
        .create(randomUserId)
        .then((v) => ({[v.id]: v}));
    });

let users;
let teams;
Promise.all(userList)
  .then((promises) => promises.reduce((p, c) => Object.assign(p, c), {}))
  .then((result) => {
    users = result;
  })
  .then(() => Promise.all(teamList))
  .then((promises) => promises.reduce((p, c) => Object.assign(p, c), {}))
  .then((result) => {
    teams = result;
  })
  .then(() => {
    console.info({
      users,
      teams,
    });
    process.exit(0);
  });
