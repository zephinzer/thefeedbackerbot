const {User} = require('../users');
const database = require('../database');
const moment = require('moment');
const {
  getErrorReporter,
} = require('../utils');

module.exports = handler;

const strings = {
  onTrigger: () => 'Please wait while we retrieve all of your feedback!',
  usersPersonalInfo: (userName, userId) =>
    '*Your feedback*\n' +
    `Your Name: *${userName}*\n` +
    `Chat ID: \`${userId}\`\n` +
    '\n',
  usersTeamRow: (fromUserName, question, answer, createdAt) =>
    `*${question}* \n
  ${answer}
  (from \`👤 ${fromUserName}\` - \`${moment(new Date(createdAt), 'YYYYMMDD').fromNow()}\`)\n\n`,
};

function handler(msg, bot) {
  const userId = msg.chat.id;
  const user = new User(database, {id: userId});
  let reply = '';
  bot.sendMessage(userId, strings.onTrigger())
    .then(() => user.loadRemote())
    .then((userData) => {
      reply += strings.usersPersonalInfo(userData.name, userId);
    }).then(() =>
      database
        .get(`feedback/${userId}`)
        .once('value', (val) => {
          const feedbackList = [];
          val.forEach((feedback) => {
            feedbackList.push(feedback.val());
          });
          if (feedbackList.length > 0) {
            reply += feedbackList.map((feedback) => {
              const question = feedback.question;
              const fromUserName = feedback.fromUserName;
              const answer = feedback.answer;
              const createdAt = feedback.createdAt;
              return strings.usersTeamRow(fromUserName, question, answer, createdAt);
            }).join('\n').trim() + '\n';
            bot.sendMessage(userId, reply, {
              parse_mode: 'Markdown',
            });
          } else {
            bot.sendMessage(userId, 'Oops, there is currently no feedback for you. 😶');
          }
        })
    ).catch(getErrorReporter('handlers/mine'));
}
