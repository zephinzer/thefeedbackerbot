const database = require('../database');
const logger = require('../logger');
const metrics = require('../metrics');
const {Team} = require('../teams');
const {
  getErrorReporter,
  responseOptions,
  waitForUserResponse,
  validateTeamName,
} = require('../utils');

const strings = {
  onExpiry: () => 'This request to create a team has expired. Use /create to initialize this command again',
  onInvalidTeamName: (teamName) =>
    `It seems like the team name you entered, \`${teamName}\` isn't friendly to our servers 🤕 ` +
    'try another name according to the following rules:\n\n' +
    '1. Has to start with an alphabet\n' +
    '2. Has to end with an alphabet\n',
  onTeamCreated: (teamName, teamId) =>
    `🎉 Your team name, *${teamName}*, has been created! ` +
    'Start inviting people to join your team by using the id: \n\n' +
    `\`${teamId}\`\n\n` +
    '💪🏼💪🏼💪🏼 here\'s to giving and receiving awesome feedback!',
  onTrigger: () =>
    'Creating a team? Awesome! Go ahead tell me the name of your team:\n\n' +
    '_(send your reply to this message if a reply prompt wasn\'t automatically created)_',
};

module.exports = handler;

handler.ID = 'HANDLER_CREATE';

function handler(msg, bot) {
  let userResponseMessageId;
  const userId = msg.chat.id;
  bot
    .sendMessage(userId, strings.onTrigger(), responseOptions.forceReply())
    .then((botResponse) => {
      const messageId = botResponse['message_id'];
      logger.info(`[handlers/create] waiting for response from ${userId}/${messageId}`);
      return waitForUserResponse({
        bot, userId, messageId, expiryMessage: strings.onExpiry(),
      });
    })
    .then((userResponse) => {
      const teamName = userResponse.text;
      userResponseMessageId = userResponse['message_id'];
      if (!validateTeamName(teamName)) {
        bot.sendMessage(userId, strings.onInvalidTeamName(teamName), responseOptions.inReplyTo(userResponseMessageId));
        throw new Error(`invalid team name "${teamName}" in message[${userResponseMessageId}] from user[${userId}]`);
      } else {
        const team = new Team(database, {name: teamName});
        return team.create(userId);
      }
    })
    .then((team) =>
      bot.sendMessage(userId, strings.onTeamCreated(team.name, team.id), responseOptions.inReplyTo(userResponseMessageId))
    )
    .then(() => metrics.record(`${handler.ID}_END`))
    .catch(getErrorReporter('handlers/create'));
}
