const os = require('os');

const {
  getErrorReporter,
} = require('../utils');

module.exports = handler;

handler.ID = 'HANDLER_DEBUG';

function handler(msg, bot) {
  bot
    .sendMessage(
      msg.chat.id,
      `
*chatId*: ${msg.chat.id}
*username*: ${msg.from.username}
*name*: ${msg.from.first_name}
*system*: ${process.platform} ${process.arch} ${os.release()}
*sys uptime*: ${Math.floor(os.uptime() / 60 / 60)} hrs
*proc uptime*: ${process.uptime()} secs
*timestamp*: ${new Date().toUTCString()}
  `.trim(),
      {
        parse_mode: 'Markdown',
      }
    )
    .catch(getErrorReporter('handlers/debug'));
}
