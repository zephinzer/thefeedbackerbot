const database = require('../database');
const metrics = require('../metrics');
const {User} = require('../users');
const {Team} = require('../teams');
const feedbackCommon = require('../common/feedback');

const FeedbackRequest = feedbackCommon.Request;

const {
  getErrorReporter,
} = require('../utils');

const strings = {
  onTrigger: () =>
    'You\'re awesome for giving feedback!\n\nGive us a moment to magically come up with someone worth giving feedback to... (:',
  onFirstContact: (userName, teamName) =>
    `Today seems like a great time to be giving feedback to *${userName}* from the team *${teamName}* which you're also in!`,
  onRetry: (userName, teamName) =>
    `How about...\n\n*${userName}* from *${teamName}*?`,
};

module.exports = handler;

handler.ID = 'HANDLER_FEEDBACK';

function handler(msg, bot) {
  const user = new User(database, {id: msg.chat.id});
  const isRetry = msg.retry ? true : false;
  return new Promise((resolve, reject) => {
    if (!isRetry) {
      bot
        .sendMessage(
          user.id,
          strings.onTrigger(),
        )
        .then(resolve)
        .catch(reject);
    } else {
      resolve();
    }
  })
    .then(() => bot.sendChatAction(user.id, 'typing'))
    .then(() => user.loadTeams())
    .then((teams) => {
      const users = Object
        .keys(teams)
        .reduce((users, teamId) =>
          users.concat(
            Object.keys(teams[teamId].users)
              .filter((userId) => userId != user.id)
              .map((userId) => ({
                userId,
                teamId,
              }))
          ), []
        );
      return users[Math.floor(Math.random() * users.length)];
    })
    .then((randomUser) => {
      const randomTeamInstance = new Team(database, {id: randomUser.teamId});
      const randomUserInstance = new User(database, {id: randomUser.userId});
      return Promise.all([
        randomUserInstance.loadRemote().then((data) => ({type: 'user', data})),
        randomTeamInstance.loadRemote().then((data) => ({type: 'team', data})),
      ]);
    })
    .then((data) => data.reduce((prev, curr) =>
      // convert the multiple promises into a hash so its more sensible to use downstream
      curr !== undefined ? Object.assign(prev, {[curr.type]: curr.data}) : prev
    , {}))
    .then((randomItem) => {
      const feedbackRequest = new FeedbackRequest({
        bot,
        fromUser: user,
      });
      return feedbackRequest.send({
        toUser: randomItem.user,
        toTeam: randomItem.team,
        message: (isRetry) ?
          strings.onRetry(randomItem.user.name, randomItem.team.name)
          : strings.onFirstContact(randomItem.user.name, randomItem.team.name),
      });
    })
    .then(() => metrics.record(`${handler.ID}_END`))
    .catch(getErrorReporter('handlers/feedback'));
}
