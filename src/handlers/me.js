const database = require('../database');
const metrics = require('../metrics');
const {User} = require('../users');
const {
  getErrorReporter,
} = require('../utils');

const strings = {
  onTrigger: () => 'Hold on while I grab all that I know about you!',
  usersPersonalInfo: (userName, userId) =>
    '*About You*\n' +
    `Name: *${userName}*\n` +
    `Chat ID: \`${userId}\`\n` +
    '\n',
  usersTeamRow: (teamId, teamName, memberCount) =>
    `${teamName} (${memberCount} 👤) - Join ID: \`${teamId}\``,
};

module.exports = handler;

handler.ID = 'HANDLER_ME';

function handler(msg, bot) {
  const userId = msg.chat.id;
  const user = new User(database, {id: userId});
  let reply = '';
  bot.sendMessage(userId, strings.onTrigger())
    .then(() => user.loadRemote())
    .then((userData) =>
      reply += strings.usersPersonalInfo(userData.name, userId)
    ).then(() =>
      database
        .get('teams')
        .orderByChild(`users/${userId}`)
        .equalTo(true)
        .once('value', (val) => {
          const teams = [];
          val.forEach((team) => {
            teams.push(team.val());
          });
          reply += '*Teams*\n';
          reply += teams.map((team) => {
            const memberCount = Object.keys(team.users).length;
            const teamName = team.name;
            const teamId = team.id;
            return strings.usersTeamRow(teamId, teamName, memberCount);
          }).join('\n').trim() + '\n';
          bot.sendMessage(userId, reply, {
            parse_mode: 'Markdown',
          });
        })
    )
    .then(() => metrics.record(`${handler.ID}_END`))
    .catch(getErrorReporter('handlers/me'));
}
