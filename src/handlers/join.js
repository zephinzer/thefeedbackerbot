const database = require('../database');
const logger = require('../logger');
const metrics = require('../metrics');
const {Team} = require('../teams');
const {
  getErrorReporter,
  responseOptions,
  waitForUserResponse,
} = require('../utils');

const strings = {
  onExpiry: () => 'This request to join a team has expired. Use /join to initialize this command again',
  onInvalidTeam: (teamId) => `No team was found with the ID "${teamId}"`,
  onTrigger: () => 'Joining a team? What’s the unique 6 digit number that your team’s creator sent you?',
  onUserAlreadyInTeam: (teamName) => `Seems like you were already in the team, ${teamName}.`,
  onUserJoinedTeam: (teamName) => `Awesome, You have joined the team, ${teamName}.`,
};

module.exports = handler;

handler.ID = 'HANDLER_JOIN';

function handler(msg, bot) {
  let team;
  const userId = msg.chat.id;
  bot
    .sendMessage(userId, strings.onTrigger(), responseOptions.forceReply())
    .then((botResponse) => {
      const messageId = botResponse['message_id'];
      logger.info(`[handlers/join] waiting for response from ${userId}/${messageId}`);
      return waitForUserResponse({
        bot, userId, messageId, expiryMessage: strings.onExpiry(),
      });
    })
    .then((userResponse) => {
      const teamIdToJoin = userResponse.text;
      if (!teamIdToJoin) {
        bot.sendMessage(userId, strings.onInvalidTeam(teamIdToJoin));
        throw new Error(`invalid team id "${team.id}"`);
      } else {
        team = new Team(database, {id: teamIdToJoin});
        return team.exists();
      }
    })
    .then((teamExists) => {
      if (!teamExists) {
        bot.sendMessage(userId, strings.onInvalidTeam(team.id));
        throw new Error(`team id "${team.id}" not found`);
      } else {
        logger.info(`[handlers/join] teamId ${team.id} exists, adding user ${userId}`);
        return team.addUser(userId);
      }
    })
    .then((created) => {
      if (created) {
        bot.sendMessage(userId, strings.onUserJoinedTeam(team.name));
      } else {
        bot.sendMessage(userId, strings.onUserAlreadyInTeam(team.name));
      }
    })
    .then(() => metrics.record(`${handler.ID}_END`))
    .catch(getErrorReporter('handlers/join'));
}
