const database = require('../database');
const logger = require('../logger');
const metrics = require('../metrics');
const {User} = require('../users');
const {
  getErrorReporter,
  responseOptions,
  validateUserName,
  waitForUserResponse,
} = require('../utils');

const strings = {
  onExpiry: () => 'This request to set your name has expired. Use /start to initialize me again ):',
  onInvalidUserName: (userName) =>
    `It seems like the name you entered, \`${userName}\` isn't friendly to our servers 🤕 `
    + 'try another name according to the following rules:\n\n'
    + '1. Has to start with an alphabet without a modifier\n'
    + '2. All alphabets including umlauts are allowed other than the first alphabet\n\n'
    + 'Run (or tap >>) /start to try again',
  onTrigger: () => 'Hi there, seems like we\'re meeting for the very first time! How do you want me to address you?',
  onUserNameChange: (userName, userUsername, userId) =>
    `Thanks *${userName}* (@${userUsername || 'unknown'}), your name is updated and your internal ID `
    + `is \`${userId || 'unknown'}\` _(use this ID when communicating errors to us for a quicker response)_.\n\n`
    + 'Type slash (/) to see the available commands. _Happy feedbacking!_',
};

module.exports = handler;

handler.ID = 'HANDLER_START';

function handler(msg, bot) {
  let user;
  let userResponseMessageId;
  const userId = msg.chat.id;
  bot
    .sendMessage(userId, strings.onTrigger(), responseOptions.forceReply())
    .then((botResponse) => {
      const messageId = botResponse['message_id'];
      logger.info(`[handlers/start] waiting for response from ${userId}/${messageId}`);
      return waitForUserResponse({
        bot, userId, messageId, expiryMessage: strings.onExpiry(),
      });
    })
    .then((userResponse) => {
      const userUsername = userResponse.chat.username || 'null';
      const userName = userResponse.text;
      userResponseMessageId = userResponse['message_id'];
      if (!validateUserName(userName)) {
        bot.sendMessage(userId, strings.onInvalidUserName(userName), responseOptions.inReplyTo(userResponseMessageId));
        throw new Error(`invalid user name "${userName}" in message[${userResponseMessageId}] from user[${userId}]`);
      } else {
        user = new User(database, {name: userName, username: userUsername, id: userId});
        return user.create();
      }
    }).then(() =>
      bot.sendMessage(
        userId,
        strings.onUserNameChange(user.name, user.username, user.id),
        responseOptions.inReplyTo(userResponseMessageId),
      )
    )
    .then(() => metrics.record(`${handler.ID}_END`))
    .catch(getErrorReporter('handlers/start'));
}
