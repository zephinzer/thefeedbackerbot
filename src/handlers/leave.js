const database = require('../database');
const logger = require('../logger');

const strings = {
  onTrigger: () => {
    return 'Leaving a team? 🤐 Which?';
  },
  notOnAnyTeam: () => {
    return 'You don\'t belong to any team. /join one first?';
  },
};

module.exports = handler;

handler.ID = 'HANDLER_LEAVE';

function handler(msg, bot) {
  const userId = msg.chat.id;
  database
    .get('teams')
    .orderByChild(`users/${userId}`)
    .equalTo(true)
    .once('value', (teams) => {
      if (teams.val()) {
        const teamOptions = [];
        teams.forEach((team) => {
          const teamName = team.val().name;
          const teamId = team.val().id;
          teamOptions.push([{text: `${teamId}: ${teamName}`, callback_data: `leave:${teamId}`}]);
        });
        const opts = {
          reply_markup: JSON.stringify({
            inline_keyboard: teamOptions,
          }),
        };
        bot.sendMessage(userId, strings.onTrigger(), opts);
      } else {
        logger.info(`[handlers/leave] ${userId} is on any team.`);
        bot.sendMessage(userId, strings.notOnAnyTeam());
      }
    });
}
