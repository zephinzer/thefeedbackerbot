const winston = require('winston');

module.exports = winston.createLogger({
  level: 'debug',
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.colorize(),
    winston.format.timestamp(),
  ),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.simple(),
      ),
    }),
  ],
});
