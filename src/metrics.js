const database = require('./database');

const MUTEX_HOLD_WAIT = 100;

module.exports = {
  init,
  record,
};

init.ID = 'SYSTEM_INIT';

/**
 * Initialises the metrics collection
 */
function init() {
  const ref = database.get(`metrics/${init.ID}`);
  ref.once('value', (initCount) => {
    ref.set((initCount.exists()) ? (initCount.val() + 1) : 1);
  });
}

record.mutex = {};

/**
 * Records a metric event
 *
 * @param {String} eventName
 * @return {Promise<void>}
 */
function record(eventName) {
  const ref = database.get(`metrics/${eventName}`);
  return new Promise((resolve, reject) => {
    if (record.mutex[eventName] === undefined) {
      record.mutex[eventName] = false;
    }
    if (record.mutex[eventName] === true) {
      setTimeout(() => {
        record(eventName)
          .then(resolve)
          .catch(reject);
      }, MUTEX_HOLD_WAIT);
    }
    if (record.mutex[eventName] === false) {
      record.mutex[eventName] = true;
      ref.once('value', (event) => {
        ref.set((event.exists()) ? (event.val() + 1) : 1)
          .then(() => {
            record.mutex[eventName] = false;
          })
          .then(resolve)
          .catch(reject);
      });
    }
  });
};
