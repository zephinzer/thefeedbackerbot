const fs = require('fs');
const path = require('path');

const convict = require('convict');

const commandsData = fs
  .readFileSync(path.join(__dirname, '../data/commandslist.txt'))
  .toString()
  .split('\n');
const commands = commandsData.map((value) => value.split(' ')[0]);

module.exports = convict({
  env: {
    doc: 'environment we rolling in',
    default: 'development',
    env: 'NODE_ENV',
  },
  i18n: {
    doc: 'international language code to use for strings',
    default: 'en',
    env: 'I18N',
  },
  commands: {
    doc: 'commands',
    default: commands,
    env: 'COMMANDS',
  },
  messageTimeout: {
    doc: 'timeout duration in milliseconds for a command response',
    default: 60000,
    env: 'MESSAGE_TIMEOUT',
  },
  dbFirebaseUrl: {
    doc: 'database url',
    default: 'https://unknown.firebaseio.com',
    env: 'DB_FIREBASE_URL',
  },
  tokenTelegram: {
    doc: 'telegram token from the botfather',
    default: null,
    env: 'TOKEN_TELEGRAM',
  },
  firebaseJsonKey: {
    doc: 'BASE64 encoded string of Firebase Service Key',
    default: null,
    env: 'FIREBASE_SERVICE_ACCOUNT_BASE64',
  },
  cronSchedule: {
    doc: 'cron schedule',
    default: '0 18 * * *', // will run at 6pm everyday where people allegedly end work
    env: 'CRON_SCHEDULE',
  },
  cronNotificationCountSchedule: {
    doc: 'cron notification count schedule',
    default: '0 9 * * 1', // will run at 9am every week before people start their work
    env: 'CRON_NOTIFICATION_COUNT_SCHEDULE',
  },
  timeIntervalForFeedbackPrompt: {
    doc: 'Time interval before next feedback prompt in ms',
    default: 604800000, // 60 * 60 * 1000 * 24 * 7
    env: 'TIME_INTERVAL_FOR_FEEDBACK_PROMPT',
  },
});
