const uuid = require('uuid/v4');
const logger = require('./logger');
const {
  getErrorReporter,
  randomFixedInteger,
} = require('./utils');

module.exports = {
  Feedback,
  FeedbackEntry,
};

Feedback.init = async (database) => {
  logger.debug('[feedback] initialising Feedback model (see feedback-readme collection)');
  const collectionRef = database.get('feedback-readme');
  const exampleRef = database.get('feedback-readme/example');
  const exampleFeedback =
    (new FeedbackEntry(database, {
      id: uuid(),
      forUserId: randomFixedInteger(9),
      fromUserId: randomFixedInteger(9),
      fromUserName: 'from user\'s name',
      teamId: randomFixedInteger(6),
      teamName: 'team\'s name',
      question: 'some question?',
      answer: 'some answer!',
      type: 'qualitative',
    })).serialize();
  collectionRef.once('value', async (feedbackReadme) => {
    let exampleExists = false;
    let modelChanged = false;
    await exampleRef.once('value', (example) => {
      exampleExists = example.exists();
      if (exampleExists) {
        const remoteExample = example.val();
        Object.keys(exampleFeedback).forEach((key) => {
          if (typeof exampleFeedback[key] !== typeof remoteExample[key]) {
            modelChanged = true;
          }
        });
      }
    });
    if (!feedbackReadme.exists() || !exampleExists || modelChanged) {
      logger.info('[feedback] Feedback model had updates, updating remote state');
      await collectionRef.set({
        description: 'contains feedback - each item in feedback indicates the user\'s id, and each of these items is an array containing all the feedback they\'ve received',
        lastModified: (new Date()).toISOString(),
      });
      try {
        await exampleRef.set(exampleFeedback);
      } catch (ex) {
        getErrorReporter('feedback/init')(ex);
      }
    }
  });
};

function Feedback(database, {
  userId,
}) {
  this.database = database;
  this.userId = userId;
  this.data = [];
  return this;
}

Feedback.prototype.getRef = function() {
  return this.database.get(`feedback/${this.userId}`);
};

Feedback.prototype.loadRemote = function() {
  return new Promise((resolve, reject) => {
    this.getRef()
      .once('value', (data) => {
        this.data = data.val();
        resolve(this);
      })
      .catch(reject);
  });
};

/**
 * @param {FeedbackEntry} feedbackEntry
 * @return {Promise<Feedback>}
 */
Feedback.prototype.add = function(feedbackEntry) {
  return new Promise((resolve, reject) => {
    const serializedFeedbackEntry = feedbackEntry.serialize();
    this.getRef()
      .push(serializedFeedbackEntry)
      .then(() => {
        this.data.push(serializedFeedbackEntry);
        resolve(this);
      })
      .catch(reject);
  });
};


Feedback.prototype.retrieveFeedbackAfterDate = function(dateOfInformedFeedbackCount) {
  return new Promise((resolve, reject) => {
    this.getRef()
      .orderByChild('createdAt').startAt(dateOfInformedFeedbackCount || 0)
      .once('value', (val) => {
        const feedbackList = [];
        val.forEach((feedback) => {
          feedbackList.push(feedback.val());
        });
        this.data = feedbackList;
        resolve(this);
      }).catch(reject);
  });
};

function FeedbackEntry(database, {
  id,
  fromUserId,
  fromUserName,
  forUserId,
  teamName,
  teamId,
  question,
  answer,
  type,
}) {
  this.id = id || uuid();
  this.database = database;
  this.fromUserId = fromUserId === undefined ? 'anonymous' : fromUserId;
  this.fromUserName = fromUserName === undefined ? 'Anonymous' : fromUserName;
  this.forUserId = forUserId;
  this.teamName = teamName;
  this.teamId = teamId;
  this.question = question;
  this.answer = answer;
  this.type = type;
  this.feedback = new Feedback(this.database, {userId: forUserId});
  return this;
};

FeedbackEntry.prototype.add = function() {
  return this.feedback
    .getRef()
    .push(this.serialize())
    .then(() => {
      logger.info(`[feedback] added feedback[${this.id}]`);
    })
    .then(() => this)
    .catch(getErrorReporter('feedback'));
};

FeedbackEntry.prototype.serialize = function() {
  return {
    id: this.id,
    fromUserId: this.fromUserId,
    fromUserName: this.fromUserName,
    teamName: this.teamName,
    teamId: this.teamId,
    question: this.question,
    answer: this.answer,
    type: this.type,
    createdAt: (new Date()).getTime(),
    createdAtISO: (new Date()).toISOString(),
  };
};
