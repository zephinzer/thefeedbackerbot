const logger = require('../logger');
const {Answer} = require('../answer');
const {
  responseOptions,
  waitForUserResponse,
} = require('../utils');

module.exports = {Question};

Question.data = require('../../data/questions.json');
Question.strings = {
  onExpiry: () => '',
};

function Question({
  bot,
  userGivingFeedback,
  userReceivingFeedback,
  team,
}) {
  this.answers = [];
  this.bot = bot;
  this.userGivingFeedback = userGivingFeedback;
  this.userReceivingFeedback = userReceivingFeedback;
  this.team = team;
  return this;
}

Question.prototype.send = function(index) {
  logger.info(`[common/feedback] sending question ${index}`);
  return new Promise((resolve, reject) => {
    if (index === Question.data.length) {
      resolve(this);
    } else {
      const personalisedMessage =
        Question.data[index].text.replace(
          '${name}',
          this.userReceivingFeedback.name
        );
      this.bot
        .sendMessage(
          this.userGivingFeedback.id,
          personalisedMessage,
          responseOptions.forceReply(),
        )
        .then((botResponse) => {
          const questionMessageId = botResponse['message_id'];
          logger.info(`[common/feedback] waiting for response from ${this.userGivingFeedback.id}/${questionMessageId}`);
          return waitForUserResponse({
            bot: this.bot,
            userId: this.userGivingFeedback.id,
            messageId: questionMessageId,
            expiryMessage: Question.strings.onExpiry(),
          });
        })
        .then((userResponse) => {
          this.answers.push(new Answer({
            question: personalisedMessage,
            answer: userResponse.text,
          }));
          resolve(this);
        });
    }
  });
};
