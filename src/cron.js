const database = require('./database');
const logger = require('./logger');
const {Feedback} = require('./feedback');
const metrics = require('./metrics');
const feedbackHandler = require(`./handlers/feedback`);
const config = require('./config');

const {
  getErrorReporter,
} = require('./utils');

const SENDING_INTERVAL_IN_MS = 30000;

const strings = {
  informNumberOfFeedbackPrompt: (numberOfFeedback) => `Hello, you got ${numberOfFeedback} new feedback. Enter /mine to view.`,
};

module.exports = {
  runCronJob,
};

function runCronJob({bot, type}) {
  logger.info(`[msg] Cron is running`);
  let users;
  database.get('users')
    .once('value', (data) => {
      users = data.val();
      (type === 'showCount') ?
        runCronNotificationForEachUser({users, bot}) : runCronForEachUser({users, bot});
    })
    .catch(getErrorReporter('cron/runCronJob'));
}

function runCronNotificationForEachUser({users, bot, currentUserKey = 0}) {
  setTimeout(() => {
    const numberOfUsers = Object.keys(users).length;
    if (currentUserKey < numberOfUsers) {
      const userId = Object.keys(users)[currentUserKey];
      const dateOfInformedFeedbackCount = users[userId].dateOfInformedFeedbackCount;
      const feedback = new Feedback(database, {userId});
      feedback.retrieveFeedbackAfterDate(dateOfInformedFeedbackCount)
        .then((feedbackList) => {
          if (feedbackList.data.length > 0) {
            sendMessageToUserAndUpdateField({
              userId,
              message: strings.informNumberOfFeedbackPrompt(feedbackList.data.length),
              bot,
              field: 'dateOfInformedFeedbackCount',
            });
          }
        })
        .catch(getErrorReporter('cron/runCronNotificationForEachUser'));
      runCronNotificationForEachUser({users, bot, currentUserKey: currentUserKey + 1});
    }
  }, SENDING_INTERVAL_IN_MS);
}

function runCronForEachUser({users, bot, currentUserKey = 0}) {
  setTimeout(() => {
    const numberOfUsers = Object.keys(users).length;
    const dateNow = new Date();

    if (currentUserKey < numberOfUsers) {
      const userId = Object.keys(users)[currentUserKey];
      const dateOfFeedback = users[userId].dateOfFeedback;
      if (!dateOfFeedback || (dateNow - new Date(dateOfFeedback) > config.get('timeIntervalForFeedbackPrompt'))) {
        sendFeedbackPrompt({
          userId,
          bot,
          field: 'dateOfFeedback',
        });
      }
      runCronForEachUser({users, bot, currentUserKey: currentUserKey + 1});
    }
  }, SENDING_INTERVAL_IN_MS);
}

function sendFeedbackPrompt({userId, bot, field}) {
  const now = new Date().getTime();
  const msg = {
    chat: {
      id: userId,
    },
  };
  metrics.record(feedbackHandler.ID || 'UNKNOWN_HANDLER');
  return feedbackHandler(msg, bot)
    .then(() => {
      return database.get(`users/${userId}/${field}`).set(now);
    })
    .then(() => {
      logger.info(`[msg] Feedback prompt sent to <${userId}> on ${now}>`);
    })
    .catch(getErrorReporter('cron/sendFeedbackPrompt'));
}

function sendMessageToUserAndUpdateField({userId, message, bot, field}) {
  const now = new Date().getTime();
  bot.sendMessage(userId, message)
    .then(() => {
      return database.get(`users/${userId}/${field}`).set(now);
    })
    .then(() => {
      logger.info(`[msg] Feedback prompt sent to <${userId}> on ${now}>`);
    })
    .catch(getErrorReporter('cron/sendMessageToUserAndUpdateField'));
}
