const {expect} = require('chai');
const utils = require('./utils');

describe('utils', () => {
  describe('randomFixedInteger()', () => {
    const {randomFixedInteger} = utils;
    const maxLength = 8;

    it(`works as expected for lengths up to ${maxLength}`, () => {
      for (let i = 1; i < maxLength; ++i) {
        expect(randomFixedInteger(i)).to.match(/^[0-9]+$/gi);
        expect(randomFixedInteger(i)).to.have.length(i);
      }
    });
  });

  describe('validateTeamName()', () => {
    const {validateTeamName} = utils;
    const disallowedCharacters = '`@*_-/';

    it('disallows telegram command characters at the start of a team name', () => {
      disallowedCharacters.split('').forEach((character) => {
        const testCase =
          character
          + Math.random().toString(32).substr(2, 6);
        expect(
          validateTeamName(testCase),
          `${testCase} yielded true when it shouldn't have`
        ).to.equal(false);
      });
    });

    it('works as expected for commonly found names', () => {
      expect(validateTeamName('MyCareersFuture')).to.equal(true);
      expect(validateTeamName('My Careers Future')).to.equal(true);
      expect(validateTeamName('Area 51 👻')).to.equal(true);
    });
  });

  describe('validateUserName()', () => {
    const {validateUserName} = utils;
    const disallowedCharacters = '1234567890`~!@#$%^&*()_+-=[{]}\\|;:\'",<.>/?';

    it('allows only alphabets', () => {
      disallowedCharacters.split('').forEach((character) => {
        const testCase =
          Math.random().toString(32).substr(2, 6)
          + character
          + Math.random().toString(32).substr(2, 6);
        expect(
          validateUserName(testCase),
          `${testCase} yielded true when it shouldn't have`
        ).to.equal(false);
      });
    });

    it('works as expected for common names', () => {
      expect(validateUserName('zephinzer')).to.equal(true);
      expect(validateUserName('Monica Chua')).to.equal(true);
      expect(validateUserName('Keith Chan Shao Rong')).to.equal(true);
      expect(validateUserName('SömËonË')).to.equal(true);
    });
  });
});
