const logger = require('./logger');

const cache = {
  data: [],
};

module.exports = {
  data: cache,
  push,
  remove,
  read,
};

function read(id) {
  logger.debug(`[cache] retrieving cache[${id}]`);
  const responseWaiterIndex = cache.data.findIndex((v) => v.id === id);
  return cache.data[responseWaiterIndex];
}

function remove(id) {
  const responseWaiterIndex = cache.data.findIndex((v) => v.id === id);
  if (responseWaiterIndex === -1) {
    logger.debug(`[cache] cache[${id}] could not be found`);
    return;
  }
  if (typeof cache.data[responseWaiterIndex].cancel === 'function') {
    cache.data[responseWaiterIndex].cancel();
  }
  cache.data.splice(responseWaiterIndex, 1);
  logger.debug(`[cache] removed cache[${id}]`);
}

function push(id, cancel, data) {
  const cacheItem = data ? data : null;
  cache.data.unshift({id, cancel, data: cacheItem});
  logger.debug(`[cache] added cache[${id}]`);
}
