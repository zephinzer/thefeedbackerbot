const firebaseAdmin = require('firebase-admin');
const config = require('./config');
const logger = require('./logger');
const firebaseCredentials = JSON.parse(
  new Buffer(config.get('firebaseJsonKey'), 'base64')
);
firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(firebaseCredentials),
  databaseURL: config.get('dbFirebaseUrl'),
});

module.exports = {
  get: (ref) => {
    const refString = `thefeedbackerbot-${config.get('env')}/${ref}`;
    logger.debug(`[database] retrieving reference for "${refString}"`);
    return firebaseAdmin.database().ref(refString);
  },
};
