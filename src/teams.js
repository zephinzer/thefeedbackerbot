const logger = require('./logger');
const {
  getErrorReporter,
  randomFixedInteger,
} = require('./utils');

module.exports = {
  Team,
};

Team.init = async (database) => {
  logger.debug('[teams] initialising Team model (see teams-readme collection)');
  const collectionRef = database.get('teams-readme');
  const exampleRef = database.get('teams-readme/example');
  const exampleTeam =
    (new Team(database, {
      id: randomFixedInteger(6),
      name: 'team name',
      users: {
        [randomFixedInteger(6)]: true,
        [randomFixedInteger(6)]: true,
        [randomFixedInteger(6)]: true,
      },
    })).serialize();
  collectionRef.once('value', async (teamsReadme) => {
    let exampleExists = false;
    let modelChanged = false;
    await exampleRef.once('value', (example) => {
      exampleExists = example.exists();
      if (exampleExists) {
        const remoteExampleTeam = example.val();
        Object.keys(exampleTeam).forEach((key) => {
          if (typeof exampleTeam[key] !== typeof remoteExampleTeam[key]) {
            modelChanged = true;
          }
        });
      }
    });
    if (!teamsReadme.exists() || !exampleExists || modelChanged) {
      logger.info('[teams] Team model had updates, updating remote state');
      await collectionRef.set({
        description: 'contains teams',
        lastModified: (new Date()).toISOString(),
      });
      try {
        await exampleRef.set(exampleTeam);
      } catch (ex) {
        getErrorReporter('team/init')(ex);
      }
    }
  });
};

/**
 * Creates a team model
 *
 * @param {Object} database
 * @param {Object} options
 * @param {String} options.name - text name
 * @param {String} options.id - 6 digit team reference
 * @param {String} options.users - hash of {...[user.id]: exists?}
 * @return {Team}
 */
function Team(database, {
  name,
  id,
  users,
}) {
  this.database = database;
  this.name = name;
  this.id = id;
  this.users = users;
  return this;
}

Team.prototype.getRef = function() {
  return this.database.get(`teams/${this.id}`);
};

Team.prototype.getName = function() {
  return new Promise((resolve, reject) => {
    this.database
      .get(`teams/${this.id}`)
      .once('value', (data) => {
        resolve(data.val().name);
      })
      .catch(reject);
  });
};

Team.prototype.loadRemote = function() {
  return new Promise((resolve, reject) => {
    try {
      this.getRef()
        .once('value', (data) => {
          const team = data.val();
          this.name = team['name'];
          this.users = team['users'];
          this.createdAt = team['createdAt'] ? team['createdAt'] : 'unavailable';
          this.createdAtISO = team['createdAtISO'] ? team['createdAtISO'] : 'unavailable';
          resolve(this);
        })
        .catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

Team.prototype.addUser = function(userId) {
  return new Promise((resolve, reject) => {
    try {
      this.database
        .get(`teams/${this.id}/users/${userId}`)
        .once('value', (data) => {
          if (!data.exists() || data.val() !== true) {
            logger.info(`user "${userId}" is not in team "${this.id}", adding now...`);
            this.database
              .get(`teams/${this.id}/users/${userId}`)
              .set(true)
              .then(() => resolve(true))
              .catch(reject);
          } else {
            logger.info(`user "${userId}" is already in team "${this.id}", nothing to do here`);
            resolve(false);
          }
        })
        .catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

Team.prototype.removeUser = function(userId) {
  return new Promise((resolve, reject) => {
    this.database
      .get(`teams/${this.id}/users/${userId}`)
      .set(null)
      .then(() => {
        resolve(true);
      })
      .catch(() => {
        reject();
      });
  });
};

/**
 * @param {String} initialUserId
 * @return {Team}
 */
Team.prototype.create = function(initialUserId) {
  return new Promise((resolve, reject) => {
    try {
      if (initialUserId === undefined) {
        throw new Error(`[team] :initialUserId was not defined when creating team[${this.id}]`);
      }
      this.id = randomFixedInteger(6);
      logger.info(`[team] attempting to create team with id "${this.id}"`);
      this.exists()
        .then((exists) => {
          if (exists) {
            logger.warn(`[team] duplicate team id "${this.id}" detected, retrying...`);
            this.create(initialUserId)
              .then(() => resolve(this))
              .catch(reject);
          } else {
            logger.info(`[team] team id "${this.id}" was not found, proceeding to create team!`);
            this.users = {[initialUserId]: true};
            return this.getRef().set(Object.assign(this.serialize(), {
              createdAt: (new Date()).getTime(),
              createdAtISO: (new Date()).toISOString(),
            }));
          }
        })
        .then(() => {
          logger.info(`[team] creation of team with id "${this.id}" was successful`);
          resolve(this);
        })
        .catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

Team.prototype.exists = function() {
  return new Promise((resolve, reject) => {
    try {
      this.getRef().once('value', (team) => {
        if (team.exists()) {
          this.name = team.val().name;
          this.users = team.val().users;
        }
        resolve(team.exists());
      }).catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

Team.prototype.serialize = function() {
  return {
    name: this.name,
    id: this.id,
    users: this.users,
  };
};
