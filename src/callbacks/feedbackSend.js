const cache = require('../cache');
const database = require('../database');
const logger = require('../logger');
const {FeedbackEntry} = require('../feedback');
const {
  getErrorReporter,
} = require('../utils');

const strings = {
  onFeedbackGiven: (userGivingFeedback, userReceivingFeedback) =>
    `Thanks, *${userGivingFeedback}*, for giving feedback to *${userReceivingFeedback}*, `
    + 'I\'m sure they\'ll appreciate it!\n\nType /feedback to give feedback to another person!',
};

module.exports = callback;

callback.ID = 'CALLBACK_FEEDBACK_SEND';

function callback(bot, callbackQuery, payload) {
  const callbackData = payload.split('.');
  const questionCacheId = callbackData[0];
  const feedback = cache.read(questionCacheId).data;
  const allFeedbackSaved = feedback.answers.map((answer) => {
    const feedbackEntry = new FeedbackEntry(
      database,
      {
        id: answer.id,
        fromUserId: feedback.userGivingFeedback.id,
        fromUserName: feedback.userGivingFeedback.name,
        forUserId: feedback.userReceivingFeedback.id,
        teamName: feedback.team.name,
        teamId: feedback.team.id,
        question: answer.question.replace(/\*/gi, ''),
        answer: answer.data,
        type: typeof answer.data,
      });
    return feedbackEntry
      .add()
      .then(() => {
        logger.info(`[callbacks/feedbackSend] feedback[${answer.id}] for user[${feedback.userReceivingFeedback.id}] by user[${feedback.userGivingFeedback.id}] was successful`);
        return answer;
      })
      .catch(getErrorReporter('callbacks/feedback'));
  });
  return bot
    .editMessageReplyMarkup(JSON.stringify({
      inline_keyboard: [],
    }), {
      chat_id: callbackQuery.message.chat.id,
      message_id: callbackQuery.message.message_id,
    }).then(() => Promise.all(allFeedbackSaved))
    .then(() =>
      bot.sendMessage(
        feedback.userGivingFeedback.id,
        strings.onFeedbackGiven(feedback.userGivingFeedback.name, feedback.userReceivingFeedback.name),
        {parse_mode: 'Markdown'},
      )
    )
    .then(() => logger.info(`[callbacks/feedback] feedback recorded for user[${feedback.userReceivingFeedback.id}] by user[${feedback.userGivingFeedback.id}]`))
    .catch(getErrorReporter('callbacks/feedback'));
};
