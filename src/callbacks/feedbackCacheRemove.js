const cache = require('../cache');
const {
  getErrorReporter,
} = require('../utils');

const strings = {
  onComplete: () => `Cool, I've discarded the feedback from that session\n\nType /feedback to give feedback to someone else!`,
};

module.exports = callback;

callback.ID = 'CALLBACK_FEEDBACK_CACHE_REMOVE';

function callback(bot, callbackQuery, payload) {
  const callbackData = payload.split('.');
  const questionCahceId = callbackData[0];
  cache.remove(questionCahceId);
  return bot
    .editMessageReplyMarkup(JSON.stringify({
      inline_keyboard: [],
    }), {
      chat_id: callbackQuery.message.chat.id,
      message_id: callbackQuery.message.message_id,
    })
    .then(() =>
      bot.sendMessage(
        callbackQuery.message.chat.id,
        strings.onComplete(),
        {parse_mode: 'Markdown'},
      )
    )
    .catch(getErrorReporter('callbacks/feedbackCacheRemove'));
};
