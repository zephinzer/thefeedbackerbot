const feedbackHandler = require('../handlers/feedback');
const {
  getErrorReporter,
} = require('../utils');

module.exports = callback;

callback.ID = 'CALLBACK_FEEDBACK_RETRY';

function callback(bot, callbackQuery, _payload) {
  return bot
    .editMessageReplyMarkup(JSON.stringify({
      inline_keyboard: [],
    }), {
      chat_id: callbackQuery.message.chat.id,
      message_id: callbackQuery.message.message_id,
    })
    .then(() =>
      feedbackHandler(Object.assign({},
        callbackQuery.message,
        {
          retry: true,
        },
      ), bot)
    )
    .catch(getErrorReporter('callbacks/feedbackRetry'));
};
