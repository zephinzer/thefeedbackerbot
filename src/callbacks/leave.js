const database = require('../database');
const logger = require('../logger');
const {Team} = require('../teams');

module.exports = callback;

const strings = {
  leftTeamSuccessfully: (teamName) => {
    return `You have left the team ${teamName}.`;
  },
};

callback.ID = 'CALLBACK_LEAVE';

function callback(bot, callbackQuery, teamId) {
  const userId = callbackQuery.message.chat.id;
  logger.info(`[callbacks/leave] ${userId} chose to leave ${teamId}.`);
  team = new Team(database, {id: teamId});
  team.removeUser(userId)
    .then(() => team.getName())
    .then((teamName) =>
      bot.sendMessage(userId, strings.leftTeamSuccessfully(teamName)))
    .catch((ex) => {
      logger.error(
        `[callbacks/leave] an error occurred: ${ex}`,
        {stack: ex.stack.split('\n').map((item) => item.trim())},
      );
    });
}
