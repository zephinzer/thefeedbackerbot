const fs = require('fs');
const path = require('path');
const logger = require('../logger');
const metrics = require('../metrics');

module.exports = {Callback};

function Callback({
  bot,
  callbackQuery,
}) {
  this.error = true;
  this.bot = bot;
  if (callbackQuery.data && typeof callbackQuery.data === 'string') {
    const data = callbackQuery.data.split(':');
    if (data.length < 2) {
      throw new Error(`[callbacks/callback] the provided callback data "${callbackQuery.data}" does not comply to <category>:<payload>`);
    }
    this._ref = callbackQuery;
    this.category = data[0];
    this.payload = data[1];
    this.id = callbackQuery.id;
    this.error = false;
  } else {
    throw new Error('[callbacks/callback] the provided :callbackQuery is not valid/was empty');
  }
  logger.debug(`[callbacks/callback] callback[${this.id}] created successfully`);
  return this;
}

Callback.prototype.run = function() {
  return new Promise((resolve, reject) => {
    try {
      if (!fs.existsSync(path.join(__dirname, `/${this.category}.js`))) {
        reject(new Error(`[callbacks/callback] the callback handler "${this.category}" does not exist`));
      } else {
        const callbackHandler = require(`./${this.category}`);
        metrics.record(callbackHandler.ID || 'UNKNOWN_CALL');
        resolve(callbackHandler(this.bot, this._ref, this.payload));
      }
    } catch (ex) {
      reject(ex);
    }
  });
};
