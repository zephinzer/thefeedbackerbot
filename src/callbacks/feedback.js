const uuid = require('uuid/v4');

const database = require('../database');
const cache = require('../cache');
const {Team} = require('../teams');
const {User} = require('../users');
const logger = require('../logger');

const {Question} = require('../question');
const {MenuItem} = require('../common/callback');
const {
  getErrorReporter,
  responseOptions,
} = require('../utils');

module.exports = callback;

const strings = {
  onConfirm: (receiverName, teamName, feedback) => `Sure you wish to give the following feedback to *${receiverName}* on their work done in *${teamName}*?\n\n${feedback}`,
  onExpiry: () => '',
  onTrigger: () => 'Grabbing our list of questions... (give us a moment)',
  remindUserWhoTheyAre: (userGivingFeedback, userReceivingFeedback) =>
    `Alright, *${userGivingFeedback}*, let's give feedback to *${userReceivingFeedback}*!`
    + '\n\n_(if a reply prompt wasn\'t automatically created for each question, right click/tap and hold, and reply to the question message)_',
};

callback.ID = 'CALLBACK_FEEDBACK';

function callback(bot, callbackQuery, payload) {
  const callbackData = payload.split('.');

  // load personalised data
  const userGivingFeedback = new User(database, {id: callbackQuery.message.chat.id});
  const userReceivingFeedback = new User(database, {id: callbackData[0]});
  const team = new Team(database, {id: callbackData[1]});
  const remoteDataLoaded = Promise.all([
    userGivingFeedback.loadRemote().then((data) => ({type: 'giver', data})),
    userReceivingFeedback.loadRemote().then((data) => ({type: 'receiver', data})),
    team.loadRemote().then((data) => ({type: 'team', data})),
  ]).then((promises) => promises.reduce((p, c) => Object.assign(p, {[c.type]: c.data}), {}));
  logger.info(`[callbacks/feedback] user[${userGivingFeedback.id}] giving feedback to user[${userReceivingFeedback.id}] for team[${team.id}]`);

  // prepare questions
  const question = new Question({
    bot,
    userGivingFeedback,
    userReceivingFeedback,
    team,
  });

  // initialise the promise chain
  let mainMessage = bot
    .editMessageReplyMarkup(JSON.stringify({
      inline_keyboard: [],
    }), {
      chat_id: callbackQuery.message.chat.id,
      message_id: callbackQuery.message.message_id,
    })
    .then(() =>
      bot.sendMessage(userGivingFeedback.id, strings.onTrigger())
    )
    .then(() => remoteDataLoaded)
    .then(() => bot.sendMessage(
      userGivingFeedback.id,
      strings.remindUserWhoTheyAre(
        userGivingFeedback.name,
        userReceivingFeedback.name,
      ),
      {
        parse_mode: 'Markdown',
      },
    ));
  // pile up the questions in the promise chain
  for (let i = 0; i < Question.data.length; ++i) {
    mainMessage = mainMessage.then(() => {
      logger.info(`[callbacks/feedback] sending user[${userGivingFeedback.id}] question index ${i}`);
      return question.send(i);
    });
  }
  // prepare a cache
  const questionCacheId = uuid();
  return mainMessage
    .then((questions) => {
      questionCache = Object.assign({}, questions, {cacheId: questionCacheId});
      cache.push(questionCacheId, null, questionCache);
      const reviewMessage = questions.answers.map((answer) =>
        `________\n${answer.question.replace(/\*/gi, '')}\nYour response: "${answer.data}"`
      ).join('\n\n');
      return bot.sendMessage(
        questions.userGivingFeedback.id,
        `Alright, ${questions.userGivingFeedback.name}, you are giving the following feedback to ${questions.userReceivingFeedback.name} from your team ${questions.team.name}:\n\n`
        + reviewMessage,
        responseOptions.sendButtons([
          (new MenuItem({
            text: 'Looks good, send it in for me now',
            data: `feedbackSend:${questionCacheId}`,
          })).serialize(),
          [
            (new MenuItem({
              text: 'Wait first (Redo)',
              data: `feedback:${userReceivingFeedback.id}.${team.id}`,
              blockButton: false,
            })).serialize(),
            (new MenuItem({
              text: 'Discard',
              data: `feedbackCacheRemove:${questionCacheId}`,
              blockButton: false,
            })).serialize(),
          ],
        ])
      );
    })
    .catch(getErrorReporter('callbacks/feedback'));
}
