const logger = require('../logger');
const {
  MenuItem,
} = require('./callback');
const {
  getErrorReporter,
  responseOptions,
} = require('../utils');

module.exports = {
  Request,
};

/**
 * Send a request to give feedback to someone
 *
 * @param {Object} opt
 * @param {TelegramBot} opt.bot
 * @param {User} opt.fromUser
 * @return {Request}
 */
function Request({
  bot,
  fromUser,
}) {
  this.bot = bot;
  this.fromUser = fromUser;
  return this;
};

/**
 * @param {Object} opt
 * @param {User} opt.toUser
 * @param {Team} opt.toTeam
 * @return {Promise<any>}
 */
Request.prototype.send = function({
  toUser,
  toTeam,
  message,
}) {
  return this.bot
    .sendMessage(
      this.fromUser.id,
      message,
      responseOptions.sendButtons([
        (new MenuItem({
          text: Request.strings.acceptThisUser(toUser.name),
          data: `feedback:${toUser.id}.${toTeam.id}`,
        })).serialize(),
        (new MenuItem({
          text: Request.strings.rejectThisUser(),
          data: `feedbackRetry:null`,
        })).serialize(),
      ], {parse_mode: 'Markdown'})
    )
    .then(() => {
      logger.info(`[common/feedback] sent feedback request to user[${this.fromUser.id}]`);
    })
    .catch(getErrorReporter('common/feedback'));
};

Request.strings = {
  acceptThisUser: (userName) => `${userName} sounds awesome 👍🏼`,
  rejectThisUser: () => 'Someone else, please? 🙊',
};
