module.exports = {
  MenuItem,
};

/**
 * @param {MenuItemOptions} options
 * @param {string} options.text
 * @param {string} options.data
 * @param {boolean} options.blockButton - when true, the button takes up an entire row
 *
 * @return {MenuItem}
 */
function MenuItem({
  text,
  data,
  blockButton = true,
}) {
  this.text = text;
  this.data = data;
  this.blockButton = blockButton;
  return this;
};

MenuItem.prototype.serialize = function() {
  const buttonData = {text: this.text, callback_data: this.data};
  return (this.blockButton) ? [buttonData] : buttonData;
};
