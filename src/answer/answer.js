const uuid = require('uuid/v4');
const logger = require('../logger');

module.exports = {Answer};

/**
 * Answer model
 *
 * @param {Object} constructorOptions
 * @param {Question} constructorOptions.question
 * @param {string} constructorOptions.answer
 *
 * @return {Answer}
 */
function Answer({
  question,
  answer,
}) {
  this.id = uuid();
  this.question = question;
  this.data = answer;
  logger.info(`[answer] answer[${this.id}] created`);
  return this;
}
