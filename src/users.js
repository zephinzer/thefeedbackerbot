const logger = require('./logger');
const {
  getErrorReporter,
  randomFixedInteger,
} = require('./utils');

module.exports = {
  User,
};

User.DOES_NOT_EXIST = 'USER_DOES_NOT_EXIST';

User.init = (database) => {
  logger.debug('[users] initialising User model (see users-readme collection)');
  const collectionRef = database.get('users-readme');
  const exampleRef = database.get('users-readme/example');
  const exampleUser =
    (new User(database, {
      id: randomFixedInteger(9),
      name: 'user\'s name',
      username: 'user\'s username',
    })).serialize();
  collectionRef.once('value', async (usersReadme) => {
    let exampleExists = false;
    let modelChanged = false;
    await exampleRef.once('value', (example) => {
      exampleExists = example.exists();
      if (exampleExists) {
        const remoteExampleUser = example.val();
        Object.keys(exampleUser).forEach((key) => {
          if (typeof exampleUser[key] !== typeof remoteExampleUser[key]) {
            modelChanged = true;
          }
        });
      }
    });
    if (!usersReadme.exists() || !exampleExists || modelChanged) {
      logger.info('[users] User model had updates, updating remote state');
      await collectionRef.set({
        description: 'contains users',
        lastModified: (new Date()).toISOString(),
      });
      try {
        await exampleRef.set(exampleUser);
      } catch (ex) {
        getErrorReporter('user/init')(ex);
      }
    }
  });
};

/**
 * Creates a user model
 *
 * @param {Object} database
 * @param {Object} opts
 * @param {String} opts.name - text name
 * @param {String} opts.username - text username
 * @param {String} opts.id - chat id
 * @param {Date} opts.dateOfFeedback - last feedback date
 * @return {User}
 */
function User(database, {
  name,
  username,
  id,
  dateOfFeedback,
}) {
  this.database = database;
  this.name = name;
  this.username = username ? username : '';
  this.id = id;
  this.dateOfFeedback = dateOfFeedback;
  return this;
}

User.prototype.create = function() {
  return this.getRef()
    .set(Object.assign(this.serialize(), {
      createdAt: (new Date()).getTime(),
      createdAtISO: (new Date()).toISOString(),
    }))
    .then(() => {
      return this;
    });
};

User.prototype.getRef = function() {
  return this.database.get(`users/${this.id}`);
};

/**
 * Returns a Promise that when resolved, returns the
 * current User instance populated with the remote
 * data
 *
 * @return {Promise<User>}
 */
User.prototype.loadRemote = function() {
  return new Promise((resolve, reject) => {
    try {
      this.getRef()
        .once('value', (value) => {
          if (!value.exists()) {
            reject(User.DOES_NOT_EXIST);
          } else {
            remoteUser = value.val();
            this.name = remoteUser['name'];
            this.username = remoteUser['username'];
            this.createdAt = remoteUser['createdAt'] ? remoteUser['createdAt'] : 'unavailable';
            this.createdAtISO = remoteUser['createdAtISO'] ? remoteUser['createdAtISO'] : 'unavailable';
          }
          resolve(this);
        })
        .catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

User.prototype.loadTeams = function() {
  return new Promise((resolve, reject) => {
    try {
      return this.database.get('teams')
        .orderByChild(`users/${this.id}`)
        .equalTo(true)
        .once('value', (teams) => {
          resolve(teams.val());
        })
        .catch(reject);
    } catch (ex) {
      reject(ex);
    }
  });
};

User.prototype.serialize = function() {
  return {
    name: this.name,
    username: this.username,
    id: this.id,
  };
};
