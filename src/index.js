const fs = require('fs');
const path = require('path');
const TelegramBot = require('node-telegram-bot-api');
const cron = require('node-cron');
const {runCronJob} = require('./cron');
const express = require('express');

const database = require('./database');
const config = require('./config');
const logger = require('./logger');
const metrics = require('./metrics');
const {User} = require('./users');
const {Team} = require('./teams');
const {Feedback} = require('./feedback');
const {Callback} = require('./callbacks');

const {
  botOptions,
  getErrorReporter,
  responseOptions,
} = require('./utils');

const expressApp = express();

const port = process.env.PORT || 3000;
expressApp.get('/', (req, res) => {
  res.send('Hello World!');
});
expressApp.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

const strings = {
  commandsAvailable: () =>
    fs.readFileSync(path.join(__dirname, '../data/commandslist.txt'))
      .toLocaleString()
      .split('\n')
      .map((commandInfo) => {
        const infoData = commandInfo.split(' - ');
        return `/${infoData[0]}: ${infoData[1]}`;
      }).join('\n'),
};

logger.debug(`[app] application environment   | ${config.get('env')}`);

logger.debug(
  `[app] firebase db url available | ${
    config.get('dbFirebaseUrl').length > 0 ? 'YES' : 'NO'
  }`
);

logger.debug(
  `[app] telegram token available  | ${
    config.get('tokenTelegram').length > 0 ? 'YES' : 'NO'
  }`
);

User.init(database);
Team.init(database);
Feedback.init(database);
metrics.init();

const handlersAvailable = fs
  .readdirSync(path.join(__dirname, '/handlers'))
  .map((handlerFilename) => handlerFilename.split('.')[0]);
const commandsOptions = [];
config.get('commands').forEach((command) => {
  if (handlersAvailable.indexOf(command) === -1) {
    logger.warn(`[app] command ${command} does not seem to be available`);
  } else {
    commandsOptions.push(command);
    logger.debug(`[app] '/${command}' registered`);
  }
});

const bot = new TelegramBot(config.get('tokenTelegram'), botOptions.polling());

cron.schedule(config.get('cronSchedule'), () => {
  runCronJob({bot});
}, {
  scheduled: true,
  timezone: 'Asia/Singapore',
});

cron.schedule(config.get('cronNotificationCountSchedule'), () => {
  runCronJob({bot, type: 'showCount'});
}, {
  scheduled: true,
  timezone: 'Asia/Singapore',
});

bot.onText(/^\/start/, (msg) => {
  const handler = require('./handlers/start');
  metrics.record(handler.ID || 'UNKNOWN_HANDLER');
  handler(msg, bot);
});
bot.onText(/^\/debug/, (msg) => {
  const handler = require('./handlers/debug');
  metrics.record(handler.ID || 'UNKNOWN_HANDLER');
  handler(msg, bot);
});

const validCommandsRegex =
  new RegExp(`^\/(${commandsOptions.join('|')})`);
bot.onText(validCommandsRegex, (msg) => {
  const user = new User(database, {id: msg.chat.id});
  const message = msg.text;
  const messageComponents = message.split(' ');
  const command = messageComponents[0].replace(/\//, '');

  user.loadRemote()
    .then(() => {
      const handler = require(`./handlers/${command}`);
      metrics.record(handler.ID || 'UNKNOWN_HANDLER');
      return handler(msg, bot);
    })
    .catch((ex) => {
      if (ex === User.DOES_NOT_EXIST) {
        require('./handlers/start')(msg, bot);
      } else {
        getErrorReporter('app')(ex);
      }
    });
});

bot.on('message', (msg) => {
  const user = new User(database, {id: msg.chat.id});
  const messageId = msg['message_id'];
  const messageText = msg.text;
  const username = msg.from.username;
  bot.sendChatAction(user.id, 'typing');
  logger.debug(`[app] message[${messageId}] from "${user.id}" (@${username}): "${messageText}"`);

  const validCommandsExtendedRegex =
    new RegExp(`^\/(${commandsOptions.concat(['start', 'debug']).join('|')})`);
  if (!msg.text.match(validCommandsExtendedRegex) && !msg['reply_to_message']) {
    bot.sendMessage(user.id,
      'I\'m sorry, I didnt understand that 😭 here\'s what I can do in the meantime:\n\n' + strings.commandsAvailable() + '\n\n' +
      '💪🏼💪🏼💪🏼 keep on giving feedback!',
      responseOptions.inReplyTo(messageId)
    );
  }
});

bot.on('callback_query', (callbackQuery) => {
  const onError = () => bot.sendMessage(
    callbackQuery.from.id,
    'It seems like I wasn\'t programmed well enough to handle that :/ my makers have been notified of this transgression of mine.'
  );
  try {
    const callback = new Callback({bot, callbackQuery});
    logger.debug(`[app] received callback[${callback.id}] for category "${callback.category}" with payload "${callback.payload}"`);
    callback.run()
      .then((done) => done)
      .then(() => {
        logger.debug(`[app] completed callback[${callback.id}]`);
      })
      .catch((ex) => {
        onError();
        getErrorReporter('app')(ex);
      });
  } catch (ex) {
    onError();
    getErrorReporter('app')(ex);
  }
});

bot.on('polling_error', (error) => {
  logger.error(error.toString());
});

bot.on('webhook_error', (error) => {
  logger.error(error.toString());
});

bot.on('error', (error) => {
  logger.error(error.toString());
  process.exit(1);
});

bot.getMe().then((data) => {
  const name = data.first_name;
  const username = data.username;
  logger.info(
    `[bot] running as '${name}' with${
      bot.isPolling() ? '' : 'out'
    } polling, url: https://t.me/${username}`
  );
});
