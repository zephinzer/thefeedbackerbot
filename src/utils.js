const cache = require('./cache');
const config = require('./config');
const logger = require('./logger');

module.exports = {
  getErrorReporter,
  expireReplyListener,
  randomFixedInteger,
  validateTeamName,
  validateUserName,
  waitForUserResponse,
  botOptions: {
    polling: () => ({
      polling: true,
    }),
  },
  responseOptions: {
    forceReply: () => ({
      parse_mode: 'Markdown',
      reply_markup: {
        force_reply: true,
      },
    }),
    inReplyTo: (messageId) => ({
      parse_mode: 'Markdown',
      reply_to_message_id: messageId,
    }),
    sendButtons: (inlineKeyboard, extraOptions = {}) =>
      Object.assign({}, extraOptions, {
        reply_markup: JSON.stringify({
          inline_keyboard: inlineKeyboard,
        }),
      }),
  },
};

function getErrorReporter(name) {
  return (ex) => {
    logger.error(`[${name}] an error occurred: ${ex}`,
      {
        stack: (ex && ex.stack) ?
          ex.stack.split('\n').map((item) => item.trim())
          : [],
      },
    );
  };
}

/**
 * Returns a with :length numerical characters
 *
 * @param {Number} length
 * @return {String}
 */
function randomFixedInteger(length) {
  const digits = '1234567890';
  let output = '';
  for (let i = 0; i < length; ++i) {
    output += digits[Math.floor(Math.random() * digits.length)];
  }
  return output;
}

/**
 * Expires a reply listener
 *
 * @param {TelegramBot} botInstance
 * @param {String} chatId
 * @param {String} listenedMessageId
 * @param {String} message
 *
 * @return {Promise<any>}
 */
function expireReplyListener(botInstance, chatId, listenedMessageId, message) {
  return new Promise((resolve, reject) => {
    if (message) {
      botInstance
        .sendMessage(
          chatId,
          message,
          {
            // don't distrub the fella,
            disable_notification: true,
            // but be passive aggressive
            reply_to_message_id: listenedMessageId,
            // and go all out while at it
            reply_markup: {
              remove_keyboard: true,
            },
          },
        )
        .then(resolve)
        .catch(reject);
    } else {
      resolve();
    }
  });
}

function validateTeamName(teamName) {
  return teamName.trim().match(/^[\`\@\*\_\-\/]/) === null;
}

function validateUserName(userName) {
  // reference to \u00C0-\u017F from https://stackoverflow.com/questions/20690499/concrete-javascript-regex-for-accented-characters-diacritics
  return userName.trim().match(/^[A-Za-z]+[a-zA-Z\ \u00C0-\u017F]*[a-z\u00C0-\u017F]+$/) !== null;
}

/**
 * Waits for a user response
 *
 * @param {Object} options
 * @param {TelegramBot} options.bot - telegram bot instance
 * @param {String} options.userId - chat id to send messages to
 * @param {String} options.messageId - id of message we're waiting for a response for
 * @param {String} options.expiryMessage - message to send user if message has expired
 * @return {Promise<Any>}
 */
function waitForUserResponse({
  bot,
  userId = '',
  messageId = '',
  expiryMessage = 'This request has expired.',
}) {
  let replyListenerCancelled = false;
  return new Promise((resolve, reject) => {
    try {
      const teamNameResponse = bot.onReplyToMessage(
        userId,
        messageId,
        (userResponse) => {
          !replyListenerCancelled &&
            bot.removeReplyListener(teamNameResponse);
          replyListenerCancelled = true;
          resolve(userResponse);
        });
      cache.push(teamNameResponse, () => replyListenerCancelled = true);
      setTimeout(() => {
        if (!replyListenerCancelled) {
          bot.removeReplyListener(teamNameResponse);
          replyListenerCancelled = true;
          expireReplyListener(bot, userId, messageId, expiryMessage);
          cache.remove(teamNameResponse);
        }
      }, config.get('messageTimeout'));
    } catch (ex) {
      reject(ex);
    }
  });
}
